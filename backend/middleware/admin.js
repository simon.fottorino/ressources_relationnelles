const User_Has_Role = require('../models/index').models.User_Has_Role;
const Role = require('../models/index').models.Role;


module.exports = (req, res, next) => {
    let isAdmin=false;
    if(req.auth.roles){
        req.auth.roles.forEach(element => {
            if(element.name=="admin"){
                isAdmin=true;
            }
        });
        if(isAdmin){
            next();
        }else{
            return res.status(400).json({error:'Pas admin'});
        }
    }else{
        return res.status(400).json({error:'Pas admin'});
    }
}