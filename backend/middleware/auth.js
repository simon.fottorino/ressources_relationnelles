const jwt = require('jsonwebtoken');
const User = require('../models/index').models.User;
const Role = require('../models/index').models.Role;
const config = require('../config/db-config');

const SECRET_APP = config.SECRET_APP_KEY;

module.exports = (req, res, next) => {
    try{
        const token = req.headers.authorization.split(' ')[1];
        const decodedToken= jwt.verify(token, SECRET_APP);
        const userId = decodedToken.userId;
        if(req.body.userId && req.body.userId !== userId){
            res.status(400).json({error:'Erreur de token'});
        }else{
            User.findOne({where : {id:userId}, include: [{model: Role}]})
            .then(user=>{
                let roles = user.roles;
                req.auth={userId, roles};
                next();
            })
            .catch(err => {
                res.status(403).json({error : 'Requête non authentifiée !'})
            });
        }
    }catch(error){
        res.status(403).json({error : 'Requête non authentifiée !'})
    }
}