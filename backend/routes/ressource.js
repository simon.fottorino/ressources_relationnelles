const express = require('express');
const router = express.Router();
const ressCtrl = require('../controllers/ressource');
const auth = require('../middleware/auth');
const admin = require('../middleware/admin');

router.get('/', ressCtrl.getRessources);

router.delete('/:id', auth, admin, ressCtrl.delRessource);


module.exports= router;