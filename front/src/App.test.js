import { render, waitFor, screen } from "@testing-library/react";
import Ressource from "./components/accueil/ressource";
import axios from "axios";

jest.mock("axios");

const testRessources = [
  {

    name: "ressource 1",
    description: "description 1",
  },
  {

    name: "ressource 2",
    description: "description 2",
  },
  {

    name: "ressource 3",
    description: "description 3",
  },
];

test("ressources list", async () => {
axios.get.mockResolvedValue({ data: testRessources });
render(<Ressource />);

expect(screen.container.querySelectorAll(".titre").length).toBe(3);
});