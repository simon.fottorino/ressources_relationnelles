import { Formik } from 'formik';
import * as axios from 'axios';
import { Redirect } from 'react-router-dom';
import { Component } from 'react';
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";
import { Container, FormWrap, FormContent, FormH1, FormLabel, FormButton } from '../login/SignInElements';



class reset extends Component{

    constructor(props){
        super(props);
        this.state={
            errorMessage:'',
            created:false
        }
    }

    submit = (values, actions) =>{
        axios.post('http://127.0.0.1:5001/user/recup',values)
        .then(response => {
            this.setState({created: true});
        })
        .catch( err => {
                if(err.response.data.error=="Utilisateur inexistant"){
                    this.setState({created: true});
                }else{
                    this.setState({errorMessage:err.response.data.error});
                }
        })
        actions.setSubmitting(false);
    }

    render(){
        return(
            <Container>
            <FormWrap className="formWrap">
            <Formik
                onSubmit={ this.submit }
                initialValues={ { mail:''} }
                validate={ this.validate }
            >
                { ({
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    values,
                    isSubmitting
                }) => (
                    <FormContent className="formContent">
                        <Form onSubmit={ handleSubmit }
                        className="form">
                            <FormH1>Mot de passe oublié</FormH1>
                            <FormLabel for="mail" className="">Adresse mail :</FormLabel>
                                <Input type="email" name="mail" values={values.mail} onChange={handleChange} onBlur={handleBlur} className="form-control"></Input>
                                <FormButton className="btn btn-primary btn-block" type="submit" value="Se connecter" disabled={ isSubmitting }>Récupérer mon mot de passe</FormButton>
                                    {this.state.errorMessage ? (
                                                <div className="danger d-flex justify-content-center">{ this.state.errorMessage }</div>
                                            ) : null}
                                    {this.state.created ? (
                                                <Redirect to={{ pathname:'/userValid', state:{msg: 'Un mail de récupération de mot de passe vous a été envoyé, veuillez le consulter svp' }}}/>
                                            ) : null}                                                      
                            
                        </Form>
                    </FormContent>
                ) }
            </Formik>
            </FormWrap>
            </Container>
        )
    }
}

export default reset;