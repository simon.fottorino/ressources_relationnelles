import { Formik } from 'formik';
import * as axios from 'axios';
import { Redirect } from 'react-router-dom';
import { Container, FormWrap, FormContent, FormH1, FormLabel, FormButton } from './SignInElements';
import Input from "react-validation/build/input";
import React, { Component } from "react";
import Form from "react-validation/build/form";
import './form.css';

class login extends Component{

    constructor(props){
        super(props);
        this.state={
            clicked:false,
            errorMessage:'',
            isLogged:false
        }
    }

    submit = (values, actions) =>{
        axios.post('http://127.0.0.1:5001/user/connect',values)
        .then(response => {
            localStorage.setItem("user", JSON.stringify(response.data));
            this.setState({isLogged: true});
            window.location.reload();
        })
        .catch( err => {
            if(err.response.data.error.name){
                this.setState({errorMessage:'Erreur'});
            }else{
                this.setState({errorMessage:err.response.data.error});
            }
        })
        actions.setSubmitting(false);
    }

    oubli = () =>{
        this.setState({clicked:true});
    }

    render(){
        return(
            <Formik
                onSubmit={ this.submit }
                initialValues={ {mail: '', PW:''} }
                validate={ this.validate }
            >
                { ({
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    values,
                    isSubmitting,
                    errors
                }) => (
                    <Container>
                    <FormWrap className="formWrap">
                        <FormContent className="formContent">
                            <Form
                                className="form"
                                onSubmit={ handleSubmit }
                            >
                                <FormH1>Connectez-vous à votre compte</FormH1>
                                <FormLabel htmlFor='username'>Email</FormLabel>
                                <Input
                                    type="email"
                                    className="form-control"
                                    name="mail"
                                    values={values.mail}
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                                <FormLabel htmlFor='password'>Mot de passe</FormLabel>
                                <Input
                                    type="password"
                                    className="form-control"
                                    name="PW"
                                    values={values.PW} 
                                    onChange={handleChange}
                                    onBlur={handleBlur}
                                />
                                <FormLabel onClick={this.oubli} href="">Mot de passe oublié?</FormLabel>
                                {errors.PW ? (
                                        <div className="danger d-flex justify-content-center">{ errors.PW }</div>
                                    ) : null}

                                {this.state.clicked ? (
                                        <Redirect to="/resetPW"/>
                                    ) : null}

                                {this.state.isLogged ? (
                                        <Redirect to="/ressources"/>
                                    ) : null}   
                                    
                                {this.state.errorMessage ? (
                                        <div className="danger d-flex justify-content-center">{ this.state.errorMessage }</div>
                                    ) : null}
                                <FormButton
                                    className="btn btn-primary btn-block"
                                    disabled={ isSubmitting }
                                    value="Se connecter"
                                    type="submit"
                                >
                                    {this.state.loading && (
                                    <span className="spinner-border spinner-border-sm"></span>
                                    )}
                                    <span>Se connecter</span>
                                </FormButton>
                            </Form>
                        </FormContent>
                    </FormWrap>
                </Container>
                ) }
            </Formik>
        )
    }
}

export default login;