import { useHistory } from 'react-router-dom';
import React, { useEffect } from 'react';
import logOut from '../../services/logOut';

function Deco() {

    const history = useHistory()

    useEffect(() => {
        logOut();
        history.push('/');
        window.location.reload();
    }, [])

    return (
        <div>
        </div>
    );
}

export default Deco;