import React from 'react';
import getCurrentId from '../../services/getCurrentId';
import axios from 'axios';
import authHeaders from '../../services/auth-header';
import SideMenu from './sideMenu';
import isLoggedIn from '../../services/isLoggedIn';
import {NavBtnLink} from '../navbar/navbarElements';
import Ressource from './ressource';

const { Component } = require("react");


class accueil extends Component{

    constructor(props){
        super(props);
        this.state={
            admin:false,
            isLogged:false,
            ressources:{}
        }
    }

    componentDidMount(){
        axios.get('http://127.0.0.1:5001/user/isadmin', { headers: authHeaders() })
            .then(response => {
                if(response.data.message){
                    this.setState({admin:true});
                }
            })
            .catch(err => {
                return false;
            });
        if(isLoggedIn()){
            this.setState({isLogged:true});
        }
        axios.get('http://127.0.0.1:5001/ressource')
        .then(response => {
            this.setState({ressources:response.data.ressource});
        })
        .catch(err => {
        });        
    }

    setStateOfParent = (axiosres) => {
        this.setState({ressources: axiosres});
    
    }


    render(){
        return(
            <div className='d-flex flex-row row-nowrap flex-fill border'>
                { this.state.isLogged && !this.state.admin ? (
                    <div className='d-flex flex-column w-20'>
                        <SideMenu id={getCurrentId()}/>
                    </div>
                ) : null}
                <div className='d-flex flex-column align-items-center flex-fill mt-15'>
                    <div className="d-flex flex-row space-around border w-65 p-15 bb">
                        <div className="d-flex flex-row w-65 justify-content-center align-items-center">
                            <input placeholder='Rechercher dans les ressources' className='lg mr-10'></input>
                            <NavBtnLink>
                                <svg xmlns="http://www.w3.org/2000/svg" width="16" height="16" fill="currentColor" class="bi bi-search" viewBox="0 0 16 16">
                                <path d="M11.742 10.344a6.5 6.5 0 1 0-1.397 1.398h-.001c.03.04.062.078.098.115l3.85 3.85a1 1 0 0 0 1.415-1.414l-3.85-3.85a1.007 1.007 0 0 0-.115-.1zM12 6.5a5.5 5.5 0 1 1-11 0 5.5 5.5 0 0 1 11 0z"/>
                                </svg>
                            </NavBtnLink>
                        </div>
                        <div className='w-35 d-flex align-items-center flex-end'>
                            <NavBtnLink>Filtres</NavBtnLink>
                        </div>
                    </div>
                    {this.state.ressources.length > 0 ? (
                        <div className='d-flex flex-column align-items-center mt-30 w-80'>
                        {this.state.ressources.map((m) => (
                            <Ressource ressource={m} admin={this.state.admin} setStateOfParent = {this.setStateOfParent} id={m.id}/>
                        ))}
                        </div>
                    ) : null}    
                </div>


            </div>
        )
    }
}

export default accueil;