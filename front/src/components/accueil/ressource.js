import * as axios from 'axios';
import authHeaders from '../../services/auth-header';
import React, { Component } from "react";
import './ressource.css';

class ressource extends Component{

    constructor(props){
        super(props);
        this.state={
            clicked:false,
            errorMessage:'',
            isLogged:false
        }
    }

    handleClick(id){
        console.log(id);
        var val = window.confirm("Voulez-vous supprimer cette ressource ?");
        if( val == true ) {
            axios.delete('http://127.0.0.1:5001/ressource/'+id, { headers: authHeaders() } )
            .then(response => {
                this.props.setStateOfParent(response.data.ressource);
            })
            .catch(error => {
                window.alert('Erreur');
            })
        }   
    }

    render(){
        return(
            <div className='d-flex border card flex-row row-nowrap w-80 p-15 mt-30'>
                {this.props.admin?(
                    <div className='w-100 d-flex flex-row row-nowrap'>
                        <div className='d-flex flex-column justify-content-center align-items-left w-80 ress'>
                            <p className='titre'>Titre : {this.props.ressource.name}</p>
                            <p className='d-flex flex-start mt-15 w-100'>Catégorie : {this.props.ressource.type.description}</p>
                            <p className='d-flex flex-start mt-15 w-100'>{this.props.ressource.shortDescription}</p>
                        </div>
                        <div className='d-flex flex-column justify-content-center align-items-center w-20'>
                            <button className='btn-supp' onClick={() => this.handleClick(this.props.id)}>Supprimer</button>
                        </div>
                    </div>        
                ):(
                    <div className='w-100 d-flex flex-row row-nowrap'>
                        <div className='d-flex flex-column justify-content-center align-items-left w-100 ress'>
                            <p className='titre'>Titre : {this.props.ressource.name}</p>
                            <p className='d-flex flex-start mt-15 w-100'>Catégorie : {this.props.ressource.type.description}</p>
                            <p className='d-flex flex-start mt-15 w-100'>{this.props.ressource.shortDescription}</p>
                        </div>
                    </div>   
                )}
            </div>     
        )
    }
}

export default ressource;