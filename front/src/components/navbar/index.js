import React from "react";
import { FaBars } from "react-icons/fa";
import {
  Nav,
  NavbarContainer,
  NavLogo,
  MobileIcon,
  NavItem,
  NavLinks,
  NavMenu,
  NavBtn,
  NavBtnLink,
} from "./navbarElements";

import isLoggedIn from '../../services/isLoggedIn';


const Navbar = ({ toggle }) => {
  return (
    <>
      <Nav>
        <NavbarContainer>
          <NavLogo to="/">RE</NavLogo>
          <MobileIcon onClick={toggle}>
            <FaBars />
          </MobileIcon>
          <NavMenu>
            <NavItem>
              <NavLinks to="">Accueil</NavLinks>
            </NavItem>
            <NavItem>
              <NavLinks to="ressources">Ressources</NavLinks>
            </NavItem>
            <NavItem>
              <NavLinks to="support">Support</NavLinks>
            </NavItem>
          </NavMenu>
          {!isLoggedIn() ? (
            <NavBtn>
              <NavBtnLink to="signin">Se connecter</NavBtnLink>
            </NavBtn>
          ):(
            <NavBtn>
              <NavBtnLink to="logOut">Se déconnecter</NavBtnLink>
            </NavBtn>
          )}
        </NavbarContainer>
      </Nav>
    </>
  );
};

export default Navbar;
