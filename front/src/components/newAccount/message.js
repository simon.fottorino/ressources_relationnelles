import { Redirect } from 'react-router-dom';
import React, { Component } from 'react';

import { Container, FormWrap, FormContent, FormH1, FormButton } from './messageElements';
import './form-message.css';


class message extends Component{
    
    constructor(props){
        super(props);
        this.state = {
            back:false
        }
    }

    handleClick = () =>{
        this.setState({back:true});
    }


    render(){
        const msg = this.props.location ? this.props.location.state.msg : 'Votre demande a été réceptionnée';
        const title= this.props.location ? this.props.location.state.title : 'Pensez à consulter vos mails ! ✉'
        return(
            
                <Container>
                    <FormWrap>
                        <FormContent>
                            <div className='form'> 
                                <FormH1>{title}</FormH1>
                                <FormH1> {msg}</FormH1>
                                <FormButton className="btn btn-primary btn-block" type="button"  onClick={this.handleClick}>
                                    Accueil
                                </FormButton>
                                { this.state.back ? (
                                    <Redirect to="/"/>
                                ) : null}
                            </div>
                        </FormContent>
                    </FormWrap>
                </Container>
           
        )
    }
}

export default message;