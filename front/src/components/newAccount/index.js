import { Formik } from 'formik';
import * as axios from 'axios';
import { Redirect } from 'react-router-dom';
import { Component } from 'react';
import { Container, FormWrap, FormContent, FormH1, FormLabel, FormButton } from '../login/SignInElements';
import '../login/form.css';
import Form from "react-validation/build/form";
import Input from "react-validation/build/input";



class create extends Component{

    constructor(props){
        super(props);
        this.state={
            errorMessage:'',
            created:false
        }
    }

    submit = (values, actions) =>{
        axios.post('http://127.0.0.1:5001/user/add',values)
        .then(response => {
            this.setState({created: true});
        })
        .catch( err => { 
            if(err.response.status==="400"){
                this.setState({errorMessage:'Utilisateur déjà existant'});
            }else{
                console.log(err);
                this.setState({errorMessage:err.response.data.message});
            }
        })
        actions.setSubmitting(false);
    }
    

    validate(values){
        let errors={};
        if(values.PW && values.PW.length < 6){
            errors.PW='Mot de passe trop court';
        }
        if(!values.firstName){
            errors.firstName='Veuillez entrer un prénom svp'
        }
        if(!values.PW){
            errors.PW='Veuillez entrer un mot de passe svp'
        }
        if(!values.lastName){
            errors.lastName='Veuillez entrer un nom svp'
        }
        if(!values.mail){
            errors.mail='Veuillez entrer une adresse mail svp'
        }
        return errors;
    }

    render(){
        return(
            <Container>
            <FormWrap className="formWrap">
            <Formik
                onSubmit={ this.submit }
                initialValues={ {lastName:'', firstName:'', mail:'', PW:''} }
                validate={ this.validate }
            >
                { ({
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    values,
                    isSubmitting,
                    errors
                }) => (
                    <FormContent className="formContent">
                        <Form onSubmit={ handleSubmit }
                        className="form">
                            <FormH1>Créer son compte</FormH1>
                                <FormLabel for="firstName" className="">Prénom :</FormLabel>
                                    <Input type="text" name="firstName" values={values.firstName} onChange={handleChange} onBlur={handleBlur} className="form-control-newAccount"></Input>
                                    {errors.firstName ? (
                                                <div className="danger d-flex justify-content-center">{ errors.firstName }</div>
                                            ) : null}
                                <FormLabel for="lastName" className="">Nom :</FormLabel>  
                                    <Input type="text" name="lastName" values={values.lastName} onChange={handleChange} onBlur={handleBlur} className="form-control-newAccount"></Input>
                                    {errors.lastName ? (
                                                <div className="danger d-flex justify-content-center">{ errors.lastName }</div>
                                            ) : null}
                                <FormLabel for="mail" className="">Adresse mail :</FormLabel>
                                    <Input type="email" name="mail" values={values.mail} onChange={handleChange} onBlur={handleBlur} className="form-control-newAccount"></Input>
                                    {errors.mail ? (
                                                <div className="danger d-flex justify-content-center">{ errors.mail }</div>
                                            ) : null}
                                <FormLabel for="password" className="">Mot de passe :</FormLabel>
                                    <Input type="password" name="PW" values={values.PW} onChange={handleChange} onBlur={handleBlur} className="form-control-newAccount"></Input>
                                    {errors.PW ? (
                                                <div className="danger d-flex justify-content-center">{ errors.PW }</div>
                                            ) : null}
                                <FormButton className="btn btn-primary btn-block" type="submit" value="Se connecter" disabled={ isSubmitting }>Créer mon compte</FormButton>
                                    {this.state.errorMessage ? (
                                                <div className="danger d-flex justify-content-center">{ this.state.errorMessage }</div>
                                            ) : null}
                                    {this.state.created ? (
                                                <Redirect to={{ pathname:'/userValid', state:{msg:'Un mail de vérification vous a été envoyé, veuillez le consulter svp'}}}/>
                                            ) : null}                                                            
                            
                        </Form>
                    </FormContent>
                ) }
            </Formik>
            </FormWrap>
            </Container>
        )
    }
}

export default create;