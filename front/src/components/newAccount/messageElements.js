import styled from 'styled-components';
import { Link } from 'react-router-dom';

export const Container = styled.div`
    position: fixed;
    top: 0;
    left: 0;
    right: 0;
    bottom: 0;
    z-index:0;
    background-color :#001f3f;
`;

export const FormWrap = styled.div`
    height: 100%;
    display: flex;
    flex-direction: column; 
    justify-content: center;
    
    @media screen and (max-width: 480px) {
        height: 80%;
    }
  
`;


export const FormContent = styled.div`
    height: 100%;
    display: flex;
    flex-direction: column; 
    justify-content: center;

    @media screen and (max-width: 480px) {
        padding:10px;
    }
`;

export const FormH1 = styled.h1`
    margin-bottom: 20px;
    color: #fff;
    font-weight: 400;
    font-size: 20px;
    text-align: center;
`;

export const FormButton = styled.button`
    background: #eb7f50;
    border: none;
    border-radius: 4px;
    color: #fff;
    font-size: 20px;
    coursor: pointer;
    padding:8px;
`;
