import React from "react";
import {
    SidebarContainer,
    Icon,
    CloseIcon,
    SidebarWrapper,
    SidebarMenu,
    SidebarLink,
    SideBtnWrap,
    SidebarRoute
} from './sidebarElements';

import isLoggedIn from '../../services/isLoggedIn';

const Sidebar = ( { isOpen, toggle}) => {
    return (
      <>
          <SidebarContainer isOpen={isOpen} onClick = {toggle}>
              <Icon onClick = {toggle}>
                  <CloseIcon />
              </Icon>
              <SidebarWrapper>
                  <SidebarMenu>
                      <SidebarLink to="" onClick = {toggle}>
                          Accueil
                      </SidebarLink>
                      <SidebarLink to="ressources" onClick = {toggle}>
                          Ressources
                      </SidebarLink>
                      <SidebarLink to="support" onClick = {toggle}>
                          Support
                      </SidebarLink>
                  </SidebarMenu>
                  {!isLoggedIn() ? (
                        <SideBtnWrap>
                        <SidebarRoute to="signin" onClick = {toggle}>Se connecter</SidebarRoute>
                        </SideBtnWrap>
                    ):(
                        <SideBtnWrap>
                        <SidebarRoute to="logOut" onClick = {toggle}>Se déconnecter</SidebarRoute>
                        </SideBtnWrap>
                    )}
              </SidebarWrapper>
          </SidebarContainer>
      </>
    );
  };
  
  export default Sidebar;
  