describe("cypress_test", () => {
    beforeEach(() => {
        cy.restoreLocalStorage()
    })

    it("#1 - Chargement de la page d'accueil", () => {
        cy.visit("")
    })

    it("#2 - Aller sur dans la création de compte", () => {
        cy.contains("Créer un compte").click()
        cy.url().should('include', '/signup')
    })

    it("#3 - Création d'un compte", () => {
        cy.get("[name='firstName']").type('thomas')
        cy.get("[name='lastName']").type('chelle')
        cy.get("[name='mail']").type('thomas.chelle@viacesi.fr')
        cy.get("[name='PW']").type('1234')

        cy.log("Erreur mot de passe trop court")
        cy.contains("Mot de passe trop court")

        cy.get("[name='PW']").clear().type('12345678')

        cy.contains("Créer mon compte").click()
    })

    it("#4 - Création réussit", () => {
        cy.url().should('include', '/userValid')
        cy.contains('Pensez à consulter vos mails !')
        cy.contains('Votre demande a été réceptionnée')
        cy.get('button').contains('Accueil')
    })

    it("#5 - Retour à l'Accueil", () => {
        cy.get('button').contains('Accueil').click()
    })

    it("#6 - Connection à mon compte", () => {
        cy.get('.sc-fujyAs').realClick()
        cy.url().should('include', '/signin')
        cy.get("[name='mail']").type('thomas.chelle@viacesi.fr')
        cy.get("[name='PW']").type('12345678')
        cy.get('button').contains("Se connecter").click()
    })

    it("#7 - Visualisation des ressources", () => {
        cy.url().should('include', '/ressources')
        cy.get('[placeholder="Rechercher dans les ressources"]').type('test')
        cy.contains('Titre :')
        cy.contains('Catégorie :')
        cy.contains('Ajouter une ressource')
        cy.contains('Toutes les ressources')
        cy.contains('Mes ressources')
        cy.contains('Mes favoris')
    })
})